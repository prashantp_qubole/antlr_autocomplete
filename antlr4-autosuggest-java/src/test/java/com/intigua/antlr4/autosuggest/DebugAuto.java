package com.intigua.antlr4.autosuggest;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static com.intigua.antlr4.autosuggest.CasePreference.*;

import java.util.Arrays;
import java.util.Collection;

import org.antlr.runtime.RecognitionException;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.tool.Grammar;
import org.antlr.v4.tool.LexerGrammar;
import org.apache.commons.lang3.StringUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DebugAuto {

    private final static String DEFAULT_LOG_LEVEL = "DEBUG";
    private LexerAndParserFactory lexerAndParserFactory;
    private Collection<String> suggestedCompletions;
    private CasePreference casePreference = null;

    @BeforeClass
    public static void initLogging() {
        System.setProperty(org.slf4j.impl.SimpleLogger.DEFAULT_LOG_LEVEL_KEY, DEFAULT_LOG_LEVEL);
        System.setProperty(org.slf4j.impl.SimpleLogger.SHOW_LOG_NAME_KEY, "false");
        System.setProperty(org.slf4j.impl.SimpleLogger.SHOW_THREAD_NAME_KEY, "false");
    }

    @Test
    public void test_mygrammar() {
        givenGrammar(MyGrammarLexer.class, MyGrammarParser.class).whenInput("MyInput").thenExpect("MyExpectedToken");
    }

    // @Test
    // public void suggest_withMultipleParseOptions_shouldSuggestAll() {
    // // Currently failing due to weird AST created by antlr4. Parser state 11
    // // has B completion token, while lexer state 11 actually generates C.
    // givenGrammar("r0: r1 | r1; r1: ('A' 'B') 'C'", "r2: 'A' ('B'
    // 'C')").whenInput("A").thenExpect("B", "BC");
    // }

    private DebugAuto givenGrammar(String... grammarLines) {
        this.lexerAndParserFactory = loadGrammar(grammarLines);
        printGrammarAtnIfNeeded();
        return this;
    }

    private DebugAuto withCasePreference(CasePreference casePreference) {
        this.casePreference = casePreference;
        return this;
    }

    /*
     * Used for testing with generated grammars, e.g. for checking out reported issues, before coming up with a more
     * focused test
     */
    protected DebugAuto givenGrammar(Class<? extends Lexer> lexerClass, Class<? extends Parser> parserClass) {
        this.lexerAndParserFactory = new ReflectionLexerAndParserFactory(lexerClass, parserClass);
        printGrammarAtnIfNeeded();
        return this;
    }

    private void printGrammarAtnIfNeeded() {
        Logger logger = LoggerFactory.getLogger(this.getClass());
        if (!logger.isDebugEnabled()) {
            return;
        }
        Lexer lexer = this.lexerAndParserFactory.createLexer(null);
        Parser parser = this.lexerAndParserFactory.createParser(null);
        String header = "\n===========  PARSER ATN  ====================\n";
        String middle = "===========  LEXER ATN   ====================\n";
        String footer = "===========  END OF ATN  ====================";
        String parserAtn = AtnFormatter.printAtnFor(parser);
        String lexerAtn = AtnFormatter.printAtnFor(lexer);
        logger.debug(header + parserAtn + middle + lexerAtn + footer);
    }

    private DebugAuto whenInput(String input) {
        AutoSuggester suggester = new AutoSuggester(this.lexerAndParserFactory, input);
        //suggester.setCasePreference(this.casePreference);
        this.suggestedCompletions = suggester.suggestCompletions();
        return this;
    }

    private void thenExpect(String... expectedCompletions) {
        assertThat(this.suggestedCompletions, containsInAnyOrder(expectedCompletions));
    }

    private LexerAndParserFactory loadGrammar(String... grammarlines) {
        String firstLine = "grammar testgrammar;\n";
        String grammarText = firstLine + StringUtils.join(Arrays.asList(grammarlines), ";\n") + ";\n";
        LexerGrammar lg;
        try {
            lg = new LexerGrammar(grammarText);
            Grammar g = new Grammar(grammarText);
            return new LexerAndParserFactory() {

                @Override
                public Parser createParser(TokenStream tokenStream) {
                    return g.createParserInterpreter(tokenStream);
                }

                @Override
                public Lexer createLexer(CharStream input) {
                    return lg.createLexerInterpreter(input);
                }
            };
        } catch (RecognitionException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
